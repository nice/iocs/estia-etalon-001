#!/usr/bin/env iocsh.bash
###############################################################################
#
# EPICS startup script to launch IOC using module to control a ETALON Multiline2
# Interferometer via its own TCP/IP control server.
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group, Lund, 2020
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
#
###############################################################################

# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require(etalonmultiline2) 

# -----------------------------------------------------------------------------
# ESTIA-Lab
# -----------------------------------------------------------------------------
#IP or hostname of the TCP endpoint.
epicsEnvSet(IPADDR,     "192.168.1.28")
#Port number for the TCP endpoint.
epicsEnvSet(IPPORT,     "2001")
#Prefix for EPICS PVs.
epicsEnvSet(PREFIX,     "ESTIA-ETALON-001")
#Name used when establishing communication
epicsEnvSet(PORTNAME,   "$(PREFIX)")
epicsEnvSet(LOCATION,   "ESTIA; $(IPADDR)")
#Polling rate to monitor configuration and measurements.
epicsEnvSet(SLOW_SCAN,  "10")
epicsEnvSet(FAST_SCAN,  "5")
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(etalonmultiline2_DIR)db")

# -----------------------------------------------------------------------------
# Specifying the TCP endpoint and port name
# -----------------------------------------------------------------------------
drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")

# -----------------------------------------------------------------------------
# Loading databases
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

# Loading ETALON Multiline2 database records (includes module, channel and group templates)
dbLoadRecords(etalonMultiLine2.db, "P=$(PREFIX),R=:, PORT=$(PORTNAME), ADDR=$(IPPORT), SLOW_SCAN=$(SLOW_SCAN), FAST_SCAN=$(FAST_SCAN)")

# -----------------------------------------------------------------------------
# Add trace...
# -----------------------------------------------------------------------------
#asynSetTraceMask($(PREFIX), -1, 0xF)
#asynSetTraceIOMask($(PREFIX), -1, 0x2)

iocInit()

